from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object('config.Config')

    swaggerui_blueprint = get_swaggerui_blueprint(
        app.config['SWAGGER_URL'],  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
        app.config['API_URL']+'.json',
        config={  # Swagger UI config overrides
            'app_name': app.config['API_TITLE']
        },
    )

    app.register_blueprint(swaggerui_blueprint, url_prefix=app.config['SWAGGER_URL'])

    with app.app_context():
        # Imports
        from . import routes

        return app