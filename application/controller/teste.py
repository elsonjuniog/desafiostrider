from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
from flask_restful_swagger_2 import Api, swagger, Schema

import rasterio
import numpy as np
import os
import json
import datetime
from osgeo import gdal,ogr,osr
import geopy.distance


def GetExtent(gt,cols,rows):
    ''' Return list of corner coordinates from a geotransform

        @type gt:   C{tuple/list}
        @param gt: geotransform
        @type cols:   C{int}
        @param cols: number of columns in the dataset
        @type rows:   C{int}
        @param rows: number of rows in the dataset
        @rtype:    C{[float,...,float]}
        @return:   coordinates of each corner
    '''
    ext=[]
    xarr=[0,cols]
    yarr=[0,rows]
    for px in xarr:
        for py in yarr:
            x=gt[0]+(px*gt[1])+(py*gt[2])
            y=gt[3]+(px*gt[4])+(py*gt[5])
            ext.append([x,y])
            #print(x,y)
        yarr.reverse()
    return ext

def centroid(vertexes):
     _x_list = [vertex [0] for vertex in vertexes]
     _y_list = [vertex [1] for vertex in vertexes]
     _len = len(vertexes)
     _x = sum(_x_list) / _len
     _y = sum(_y_list) / _len
     return(_x, _y)

def ReprojectCoords(coords,src_srs,tgt_srs):
    ''' Reproject a list of x,y coordinates.
        @type geom:     C{tuple/list}
        @param geom:    List of [[x,y],...[x,y]] coordinates
        @type src_srs:  C{osr.SpatialReference}
        @param src_srs: OSR SpatialReference object
        @type tgt_srs:  C{osr.SpatialReference}
        @param tgt_srs: OSR SpatialReference object
        @rtype:         C{tuple/list}
        @return:        List of transformed [[x,y],...[x,y]] coordinates
    '''
    trans_coords=[]
    transform = osr.CoordinateTransformation( src_srs, tgt_srs)
    for x,y in coords:
        x,y,z = transform.TransformPoint(x,y)
        trans_coords.append([x,y])
    return trans_coords

def extdate(strname):
    str_date=strname.split("-")[3].split(".")[0]
    date_time_obj = datetime.datetime.strptime(str_date, '%Y%m%dT%H%M%SZ')
    return date_time_obj.isoformat()


def ndvi_percent(filename):
    raste=rasterio.open(filename)

    red = raste.read(3).astype('float64') #red
    nir = raste.read(4).astype('float64') #nir

    #Max 1 Min 0
    ndvi=np.where(
        (nir+red)==0., 
        0, 
        (nir-red)/(nir+red))

    return np.median(ndvi)

class GeoJsonModel(Schema):
    type = 'object'
    properties = {
        'type': {
            'type': 'string'
        },
        'geometry': {
            'type': 'object'
        },
        'properties': {
            'type': 'object'
        }
    }



class VcrModel(Schema):
      title = 'Vegetation Cover Result'
      type = 'object'
      properties = {
        'filename': {
          'type': 'string',
          'description': 'The filename of the scene file used (for reference).\n'
        },
        'cover': {
          'type': 'number',
          'format': 'float',
          'description': 'The vegetation cover for the given scene (0.0 to 1.0).\n'
        },
        'area': {
          'type': 'number',
          'format': 'float',
          'description': 'Area of the inspected site in square kilometers.\n'
        },
        'centroid': GeoJsonModel,
        'local_time': {
          'type': 'string',
          'description': 'Timestamp of the capture in local time, in ISO 8601 format.\n'
        }
      }    


class Teste(Resource):
    @swagger.doc({
        'tags': ['vegetation-cover'],
        'description': 'Calculates vegetation cover and some geographical information for a given file in the server (preexisting).',
        'responses': {
            '200': {
            'description': 'Successful response',
            'schema': VcrModel
        }
     }
     })
    def get(self):
        filename='319567_2331703_2016-12-07_0c0b-20161207T151953Z.tif'
        #Usa gdalinfo
        #file_info = os.popen('gdalinfo -json ' + filename).read()
        #info_json = json.loads(file_info)
        #geoinfo = GeoJsonModel(geometry=info_json['wgs84Extent'])

        #Usa modulo gdal
        ds=gdal.Open(filename)

        gt=ds.GetGeoTransform()
        cols = ds.RasterXSize
        rows = ds.RasterYSize
        ext=GetExtent(gt,cols,rows)

        cent=centroid(ext)


        #Cálc area
        src_srs=osr.SpatialReference()
        src_srs.ImportFromWkt(ds.GetProjection())
        tgt_srs = src_srs.CloneGeogCS()
        #
        geo_ext=ReprojectCoords(ext,src_srs,tgt_srs)

        p1=(geo_ext[0][1], geo_ext[0][0])
        p2=(geo_ext[1][1], geo_ext[1][0])
        height = geopy.distance.distance(p1, p2).km

        p2=(geo_ext[2][1], geo_ext[2][0])
        width = geopy.distance.distance(p1, p2).km

        area= height * width


        #geoinfo = [GeoJsonModel(geometry={"type": "Polygon", "properties":{"name": "Corner Coordinates"} , "coordinates": ext}), GeoJsonModel(geometry={"type": "Point", "properties":{"name": "Center Coordinates"} ,"coordinates": cent})]
        geoinfo = GeoJsonModel(type="Feature", geometry={"type": "Point", "coordinates": cent})

        result = VcrModel(filename=filename, cover=ndvi_percent(filename), area=area, centroid=geoinfo, local_time=extdate(filename))
        return result, 200




