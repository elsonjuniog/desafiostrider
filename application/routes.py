from flask import request, render_template, make_response
from flask import current_app as app
from flask_restful import reqparse, abort, Resource
from flask_restful_swagger_2 import Api
from .controller.teste import Teste

#api = Api(app)
api = Api(app, api_version=app.config['API_VERSION'], api_spec_url=app.config['API_URL'], title=app.config['API_TITLE'], description=app.config['API_DESCRIPTION'], terms=app.config['API_TERMS'])

###
#Rotas Flask-API
#Rotas para teste de API e Swagger
###
api.add_resource(Teste, '/vegetation-cover')