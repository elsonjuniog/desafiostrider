## Desafio Strider

Aplicação Python usando Flask-Restful, Flask-Swagger.  

Estrutura:  
```
.  
├── app_jwt.py  
├── application  
│   ├── controller  
│   │   ├── teste.py    
│   ├── \_\_init\_\_.py    
│   ├── routes.py  
├── config.py  
├── .env.example
├── readme.md  
├── requirements.txt  
├── start.sh  
└── wsgi.py
└── Dockerfile 
```
##### config.py    
Neste arquivo constam as variáveis que devem ser declaradas no arquivo de configuração .env    
  
##### .env  
Arquivo com as configurações nescessárias ao funcionamento da aplicação  
  
##### .env.example  
Arquivo com exemplo de configurações nescessárias ao funcionamento da aplicação  
  
##### requirements.txt  
Arquivo contendo lista de modulos nescessários ao funcionamento da aplicação  
  
##### start.sh  
Arquivos com variáveis de ambiente que inicializa a aplicação  
  
##### wsgi.py   
Arquivo principal da aplicação.  
  
##### application  
Diretório contendo o código da aplicação  
  
##### application/__init__.py  
Segundo arquivo a ser executado pela aplicação, neste é carregado Sawagger  
  
##### application/routes.py  
Arquivo contendo rotas da aplicação  
  
##### controller  
Diretório contendo controller da aplicação  
  
##### controller/teste.py  
Código com a solução do desafio 

##### Dockerfile
Arquivo contendo configurações para gerar imagem Docker com o projeto
  
## Swagger
  
Nesta aplicação foi instalado o Flask-Restful-Swagger e Flask-Swagger-ui. As configuração estão no arquivo .env um exemplo de uso pode ser encontrado no arquivo controller/contratos.py.

Para acessar os arquivo swagger.json [http://127.0.0.1:5000/api/swagger.json](127.0.0.1:5000/api/swagger.json)  
Para acessar os arquivo ao Swagger-UI [http://127.0.0.1:5000/api/docs](127.0.0.1:5000/api/docs)  
  
___
Escrito por Elson Junio (elsonjuniog@gmail.com)