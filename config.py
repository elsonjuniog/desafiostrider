from os import environ


class Config:
    """Set Flask configuration vars from .env file."""

    # General Config
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_APP = environ.get('FLASK_APP')
    FLASK_ENV = environ.get('FLASK_ENV')

    # Database
    #'sqlite:////tmp/test.db'
    #postgresql://sqlalchemy:sqlalchemy@localhost/test_sqlalchemy
    #SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    #SQLALCHEMY_TRACK_MODIFICATIONS = environ.get("SQLALCHEMY_TRACK_MODIFICATIONS")

    #Users
    #AUTHENTICATION = environ.get("AUTHENTICATION", "local")
   # LDAP_URI = environ.get("LDAP_URI", "ldap://")
   # LDAP_BIND = environ.get("LDAP_BIND", "")
   # LDAP_BIND_PASSWORD = environ.get("LDAP_BIND_PASSWORD", "")
   # LDAP_BASE = environ.get("LDAP_BASE", "")
   # LDAP_QUERY = environ.get("LDAP_QUERY", "")
   # HASH = environ.get("md5sum")

    #SWAGGER
    API_TITLE = environ.get("API_TITLE", 'Desafio Strider GIS')
    API_VERSION = environ.get("API_VERSION", '1.0.0')
    SWAGGER_URL = environ.get("SWAGGER_URL", '/api/docs')
    API_URL = environ.get("API_URL", '/api/swagger')
    API_DESCRIPTION = environ.get("API_DESCRIPTION", '')
    API_TERMS = environ.get("API_TERMS", '')

