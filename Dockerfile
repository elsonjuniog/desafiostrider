FROM python:3
#ENV all_proxy="http://10.100.134.12:8080"
#ENV ftp_proxy="http://10.100.134.12:8080"
#ENV http_proxy="http://10.100.134.12:8080"
#ENV https_proxy="http://10.100.134.12:8080"
#ENV no_proxy="10.96.0.0/12,10.11.101.96/28"
#
#ENV all_proxy="http://200.198.59.244:8080"
#ENV ftp_proxy="http://200.198.59.244:8080"
#ENV http_proxy="http://200.198.59.244:8080"
#ENV https_proxy="http://200.198.59.244:8080"
#ENV no_proxy="10.96.0.0/12,10.11.101.96/28"
ENV BUILD_ID="1.0"
ADD ./ /code
WORKDIR /code
RUN apt update
RUN apt install -y build-essential python3-dev libldap2-dev libsasl2-dev ldap-utils libgdal-dev
RUN apt install -y libgdal-grass python-gdal gdal-data gdal-bin
RUN apt install -y python3-gdal libgdal20
RUN pip install -r requirements.txt
RUN pip install GDAL==$(gdal-config --version) --global-option=build_ext --global-option="-I/usr/include/gdal"
CMD bash start.sh

